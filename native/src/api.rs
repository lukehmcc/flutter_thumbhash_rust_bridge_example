use thumbhash::{thumb_hash_to_rgba};
use base64::{Engine as _, engine::general_purpose};
use image::imageops::FilterType;
use image::io::Reader as ImageReader;

pub struct ThumbnailResponse {
    pub bytes: Vec<u8>,
    pub width: usize,
    pub height: usize,
}

pub fn rgba_to_thumb_hash_bridged(
    w: u16, 
    h: u16,
    image_path: String,
) -> String
{
    let img = ImageReader::open(image_path).unwrap().decode().unwrap();
    let thumbhash_input_image = img.resize(w.into(), h.into(), FilterType::Triangle);
    let thumbhash_bytes = thumbhash::rgba_to_thumb_hash(
        thumbhash_input_image.width().try_into().unwrap(),
        thumbhash_input_image.height().try_into().unwrap(),
        &thumbhash_input_image.to_rgba8().into_raw(),
    );
    let thumbhash_string: String = general_purpose::STANDARD_NO_PAD.encode(thumbhash_bytes);
    return thumbhash_string;
}

pub fn thumb_hash_to_rgba_bridged(hash_bytes: Vec<u8>) -> ThumbnailResponse{
    let rgba: (usize, usize, Vec<u8>) = thumb_hash_to_rgba(&hash_bytes).unwrap();
    let thumb_response = ThumbnailResponse{
        bytes: rgba.2,
        width: rgba.0,
        height: rgba.1
    };
    return thumb_response;
}