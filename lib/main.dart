import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_thumbhash_rust_bridge_example/native.dart';
import 'package:flutter_thumbhash/flutter_thumbhash.dart';
import 'package:image/image.dart' as im;
import 'package:flutter/widgets.dart' as widget;
import 'dart:ui' as ui;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Photo Upload Example',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Photo Upload Example'),
        ),
        body: const PhotoUploadWidget(),
      ),
    );
  }
}

class PhotoUploadWidget extends StatefulWidget {
  const PhotoUploadWidget({Key? key}) : super(key: key);

  @override
  _PhotoUploadWidgetState createState() => _PhotoUploadWidgetState();
}

class _PhotoUploadWidgetState extends State<PhotoUploadWidget> {
  File? _imageFile;
  String? _imageHash;
  ThumbHash? _imageHashRenderedDart;
  Uint8List? _imageHashRenderedBytesRust;
  final double bottomPadding = 16.0;
  int? _dartRenderTime;
  int? _rustRenderTime;

  Future<void> _pickImage() async {
    final filePickerResult =
        await FilePicker.platform.pickFiles(type: FileType.image);
    if (filePickerResult != null && filePickerResult.files.isNotEmpty) {
      final path = filePickerResult.files.first.path!;
      final fileName = basename(path);
      final appDir = await getTemporaryDirectory();
      final savedFile = await File(path).copy('${appDir.path}/$fileName');
      setState(() {
        _imageFile = savedFile;
      });
      _convertImageToThumbHash();
    }
  }

  Future<void> _convertImageToThumbHash() async {
    if (_imageFile != null) {
      try {
        String hashed = await api.rgbaToThumbHashBridged(
            w: 100, h: 100, imagePath: _imageFile!.path);
        setState(() {
          _imageHash = hashed;
        });
        _doImageConversions();
      } catch (e) {
        print(e.toString());
      }
    }
  }

  _doImageConversions() async {
    Stopwatch stopwatch = Stopwatch()..start();
    _covertThumbhashToImageDart();
    stopwatch.stop();
    _dartRenderTime = stopwatch.elapsedMilliseconds;
    stopwatch = Stopwatch()..start();
    _covertThumbhashToImageRust();
    stopwatch.stop();
    _rustRenderTime = stopwatch.elapsedMilliseconds;
  }

  Future<void> _covertThumbhashToImageDart() async {
    if (_imageHash != null) {
      try {
        ThumbHash hashImage = ThumbHash.fromBase64(_imageHash!);
        setState(() {
          _imageHashRenderedDart = hashImage;
        });
      } catch (e) {
        print(e.toString());
      }
    }
  }

  Future<void> _covertThumbhashToImageRust() async {
    if (_imageHash != null) {
      try {
        Uint8List hashBytes = base64Decode(_imageHash!);
        ThumbnailResponse thumbResponse =
            await api.thumbHashToRgbaBridged(hashBytes: hashBytes);
        im.Image thumb = im.Image.fromBytes(
          width: thumbResponse.width,
          height: thumbResponse.height,
          bytes: thumbResponse.bytes.buffer,
          numChannels: 4,
        );
        Uint8List? imageWithHeader = im.encodeNamedImage(".bmp", thumb);
        setState(() {
          _imageHashRenderedBytesRust = imageWithHeader;
        });
      } catch (e) {
        print(e.toString());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ElevatedButton(
            onPressed: _pickImage,
            child: const Text("Upload Photo"),
          ),
          ElevatedButton(
            onPressed: _doImageConversions,
            child: const Text("Convert to image hash"),
          ),
          _imageHash != null ? Text(_imageHash!) : Container(),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text("Input Image"),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 3,
                    height: MediaQuery.of(context).size.width / 3,
                    child: _imageFile != null
                        ? Image.file(_imageFile!, fit: BoxFit.cover)
                        : const Text("No photo selected"),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _dartRenderTime != null
                      ? Text("Rendered Via Dart: $_dartRenderTime ms")
                      : Container(),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 3,
                    height: MediaQuery.of(context).size.width / 3,
                    child: _imageHashRenderedDart != null
                        ? Image(
                            image: _imageHashRenderedDart!.toImage(),
                            fit: BoxFit.cover,
                          )
                        : const Text("Not rendered"),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _rustRenderTime != null
                      ? Text("Rendered Via Rust: $_rustRenderTime ms")
                      : Container(),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 3,
                    height: MediaQuery.of(context).size.width / 3,
                    child: _imageHashRenderedBytesRust != null
                        ? Image.memory(
                            _imageHashRenderedBytesRust!,
                            fit: BoxFit.cover,
                          )
                        : const Text("Not rendered"),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: bottomPadding,
          )
        ],
      ),
    );
  }
}
