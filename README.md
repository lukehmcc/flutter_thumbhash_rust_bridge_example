# Flutter Rust Bridge Thumbhash Example

This app is a test that compares performance and viability between the [thumbhash_flutter](https://pub.dev/packages/flutter_thumbhash) dart library and the [thumbhash](https://crates.io/crates/thumbhash) rust library for use in flutter.

It currently only runs on Windows as it is for development purposes only, but the [rust bridge](https://pub.dev/packages/flutter_rust_bridge) can easily be used on any supported flutter platform.

![](screenshot.png)

Something to note here, technically the dart library is cheating here a bit on performance because it doesn't actually generate the image in the same way that the rust library does.

```dart
Future<void> _covertThumbhashToImageDart() async {
  if (_imageHash != null) {
    try {
      ThumbHash hashImage = ThumbHash.fromBase64(_imageHash!);
      setState(() {
        _imageHashRenderedDart = hashImage;
      });
    } catch (e) {
      print(e.toString());
    }
  }
}

Future<void> _covertThumbhashToImageRust() async {
  if (_imageHash != null) {
    try {
      Uint8List hashBytes = base64Decode(_imageHash!);
      ThumbnailResponse thumbResponse =
          await api.thumbHashToRgbaBridged(hashBytes: hashBytes);
      im.Image thumb = im.Image.fromBytes(
        width: thumbResponse.width,
        height: thumbResponse.height,
        bytes: thumbResponse.bytes.buffer,
        numChannels: 4,
      );
      Uint8List? imageWithHeader = im.encodeNamedImage(".bmp", thumb);
      setState(() {
        _imageHashRenderedBytesRust = imageWithHeader;
      });
    } catch (e) {
      print(e.toString());
    }
  }
}
```

But even when accounting for this, the performance difference is 1-2ms tops. In conclusion, just use the native library it's probably fine.